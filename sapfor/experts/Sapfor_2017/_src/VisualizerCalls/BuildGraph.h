#pragma once

std::map<std::string, std::pair<int, int>> buildLocationOfGraph(const std::map<std::string, std::vector<FuncInfo*>>& allFuncInfo, const int iters = 1000, const int coef = 100, const unsigned int width = 1920, const unsigned int height = 1080);