      program loops_combiner_test
      implicit none
      parameter (l = 16,m = 6)
      real :: a(l),b(l),c(l)

! should be combined:
      do it1 = 1,m
         do k1 = 1,l
            a(k1) = it1 + k1
         enddo
      enddo 

      do it2 = 1,m
         c(it2) = it2
      enddo 

! should be combined:
      do it3 = 1,l
         c(it3) = it3
      enddo 

      do it4 = 1,l
         do k4 = 1,m
            a(k4) = it4 + k4
         enddo
      enddo 

      end

