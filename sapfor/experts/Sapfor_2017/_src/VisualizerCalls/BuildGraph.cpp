#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>

#include <string>
#include <fstream>
#include <iostream>
#include <algorithm>

#include <vector>
#include <map>
#include <set>
#include <utility>
#include <assert.h>

#include "../GraphCall/graph_calls_func.h"
#include "graphLayout/nodesoup.hpp"
#include "BuildGraph.h"

using std::map;
using std::vector;
using std::string;
using std::pair;
using std::set;

using namespace nodesoup;

static int getNextRandNum(int &N, const int SIZE, int curr)
{
    int ret = N % SIZE;
    ++N;
    while (ret == curr)
    {
        if (SIZE == 1)
            break;
        ret = N % SIZE;
        ++N;
    }

    return ret;
}

map<string, pair<int, int>>
buildLocationOfGraph(const map<string, vector<FuncInfo*>>& allFuncInfo, const int iters, const int coef, const unsigned int width, const unsigned int height)
{
    map<string, FuncInfo*> mapOfFuncs;
    createMapOfFunc(allFuncInfo, mapOfFuncs);

    map<string, int> num;
    
    int z = 0;
    for (auto& elem : mapOfFuncs)
        num[elem.first] = z++;

    for (auto& elem : mapOfFuncs)
        for (auto& callFrom : elem.second->detailCallsFrom)
            if (num.find(callFrom.first) == num.end())
                num[callFrom.first] = z++;

    adj_list_t g;
    g.resize(z);

    vector<Point2D> positions;
    vector<double> radiuses = size_radiuses(g);

    set<pair<string, string>> edges;
    for (auto& elem : mapOfFuncs)
    {
        for (auto& callFrom : elem.second->detailCallsFrom)
        {
            const pair<string, string> key = make_pair(elem.first, callFrom.first);
            if (edges.find(key) == edges.end())
            {
                edges.insert(key);
                g[num[elem.first]].push_back(num[callFrom.first]);
                g[num[callFrom.first]].push_back(num[elem.first]);
            }
        }
    }

    positions = fruchterman_reingold(g, width, height, iters, coef);

    map<string, pair<int, int>> result;
    for (auto& elem : num)
        result[elem.first] = positions[num[elem.first]].toPair();
    
    return result;
}